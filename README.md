To use this application, 

1. make a dev.js file inside config folder 
2. put link to mongoDB into the dev.js file 
3. Type  " npm install " & "npm install --dev" inside the root directory  ( Download Server Dependencies ) 
4. Type " npm install " inside the client directory ( Download Front-end Dependencies )

Run in root directory: 

```
npm run dev
```